import { ApolloProvider, HttpLink, ApolloClient, InMemoryCache, gql } from '@apollo/client';
import React, { createContext, ReactChild, ReactChildren, useContext, useState } from 'react';

interface LogIn {
  username: string;
  password: string;
}

interface AuxProps {
  children: ReactChild | ReactChildren;
}

const authContext = createContext<any>({});

export function AuthProvider({ children }: AuxProps) {
  const a = useProvideAuth();
  return (
    <authContext.Provider value={a}>
      <ApolloProvider client={a.getApolloClient()}>
        {children}
      </ApolloProvider>
    </authContext.Provider>

  );
};

export const useAuth = () => {
  return useContext(authContext)
}

const useProvideAuth = () => {
  const [token, setToken] = useState(null);

  const isLogIn = () => {
    return token ? true : false;
  }

  const getAuthHeader = () => {
    if (!token) return null;

    return {
      authorization: `Bearer ${token}`,
    }
  }

  const getApolloClient = () => {
    const link = new HttpLink({
      uri: 'http://localhost:5000/graphql',
      headers: getAuthHeader(),
    })

    return new ApolloClient({
      link,
      cache: new InMemoryCache,
    })
  }

  const logIn = async ({ username, password }: LogIn) => {
    const client = getApolloClient();

    const LoginMutation = gql`
      mutation login($username: String!, $password: String!){
        logIn(username: $username, password: $password){
          user{
            _id
            username
            name
            email
          }
          token
        }
      }
    `;

    const result = await client.mutate({
      mutation: LoginMutation,
      variables: { username, password },
    })

    
    if (result?.data?.login?.token) {
      setToken(result.data.login.token)
    }
    console.log(result.data)
  }

  const signOut = () => {
    setToken(null)
  }

  return {
    setToken,
    isLogIn,
    logIn,
    signOut,
    getApolloClient,
  }
};